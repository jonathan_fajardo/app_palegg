//
//  CarritoViewController.h
//  Palegg
//
//  Created by Jonathan Fajardo Roa on 12/02/17.
//  Copyright © 2017 Jonathan Fajardo Roa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "empresa.h"
#import "producto.h"
#import "AppDelegate.h"

@interface CarritoViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, NSURLConnectionDelegate>

@property (nonatomic, strong) AppDelegate * appD;
- (IBAction)cerrar:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_empresa_nombre;
@property (weak, nonatomic) IBOutlet UITableView *tabla;
- (IBAction)ordenar:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_cant_carrito;
@property (weak, nonatomic) IBOutlet UILabel *lbl_total_carrito;

@end
