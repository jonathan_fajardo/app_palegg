//
//  CarritoViewController.m
//  Palegg
//
//  Created by Jonathan Fajardo Roa on 12/02/17.
//  Copyright © 2017 Jonathan Fajardo Roa. All rights reserved.
//

#import "CarritoViewController.h"

@interface CarritoViewController ()

@end

@implementation CarritoViewController
@synthesize tabla, lbl_cant_carrito, lbl_total_carrito, lbl_empresa_nombre, appD;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appD = [[UIApplication sharedApplication]delegate];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"celda"];
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cerrar:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)ordenar:(id)sender {
    
}

@end
