//
//  CrearCuentaViewController.h
//  Palegg
//
//  Created by Jonathan Fajardo Roa on 31/01/17.
//  Copyright © 2017 Jonathan Fajardo Roa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CrearCuentaViewController : UIViewController

@property (nonatomic, strong) NSMutableData * acumulaDatos;
@property(nonatomic, strong)UITextField * campoActivo;

- (IBAction)volver:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txt_nombre;
@property (weak, nonatomic) IBOutlet UITextField *txt_correo;
@property (weak, nonatomic) IBOutlet UITextField *txt_clave;
@property (weak, nonatomic) IBOutlet UITextField *txt_clave_confirma;
@property (weak, nonatomic) IBOutlet UIButton *btn_registrarme;
- (IBAction)registrarme:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end
