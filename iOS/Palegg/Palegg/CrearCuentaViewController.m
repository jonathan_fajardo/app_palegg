//
//  CrearCuentaViewController.m
//  Palegg
//
//  Created by Jonathan Fajardo Roa on 31/01/17.
//  Copyright © 2017 Jonathan Fajardo Roa. All rights reserved.
//

#import "CrearCuentaViewController.h"

@interface CrearCuentaViewController ()

@end

@implementation CrearCuentaViewController
@synthesize scrollView, acumulaDatos, campoActivo, txt_correo, txt_clave, txt_nombre, txt_clave_confirma, btn_registrarme;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    txt_correo.leftView = paddingView;
    txt_correo.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    txt_nombre.leftView = paddingView2;
    txt_nombre.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    txt_clave.leftView = paddingView1;
    txt_clave.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    txt_clave_confirma.leftView = paddingView3;
    txt_clave_confirma.leftViewMode = UITextFieldViewModeAlways;
    
    UIColor *color = [UIColor lightGrayColor];
    txt_nombre.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Nombre completo" attributes:@{NSForegroundColorAttributeName:color}];
    
    txt_correo.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Correo electrónico" attributes:@{NSForegroundColorAttributeName:color}];
    
    txt_clave.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Contraseña" attributes:@{NSForegroundColorAttributeName:color}];
    
    txt_clave_confirma.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirmar contraseña" attributes:@{NSForegroundColorAttributeName:color}];
    
    txt_nombre.layer.cornerRadius = 10;
    txt_nombre.clipsToBounds = YES;
    
    txt_correo.layer.cornerRadius = 10;
    txt_correo.clipsToBounds = YES;
    
    txt_clave.layer.cornerRadius = 10;
    txt_clave.clipsToBounds = YES;
    
    txt_clave_confirma.layer.cornerRadius = 10;
    txt_clave_confirma.clipsToBounds = YES;
    
    btn_registrarme.layer.cornerRadius = 10;
    btn_registrarme.clipsToBounds = YES;
    
    
    //Detección de toques en el scroll view
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewPulsado)];
    [tapRecognizer setCancelsTouchesInView:NO];
    [scrollView addGestureRecognizer:tapRecognizer];
    
    [scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
    
    
    //Notificaciones del teclado
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(apareceElTeclado:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(desapareceElTeclado:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

#pragma mark - Notificaciones del teclado
- (void) apareceElTeclado:(NSNotification *)laNotificacion {
    NSDictionary *infoNotificacion = [laNotificacion userInfo];
    CGSize tamanioTeclado = [[infoNotificacion objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, tamanioTeclado.height, 0);
    [scrollView setContentInset:edgeInsets];
    [scrollView setScrollIndicatorInsets:edgeInsets];
    [scrollView scrollRectToVisible:[self campoActivo].frame animated:YES];
    [scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + tamanioTeclado.height)];
}

- (void) desapareceElTeclado:(NSNotification *)laNotificacion {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    [scrollView setContentInset:edgeInsets];
    [scrollView setScrollIndicatorInsets:edgeInsets];
    [UIView commitAnimations];
    
    [scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
    
    [self scrollViewPulsado];
}

- (void) scrollViewPulsado {
    [[self view] endEditing:YES];
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)volver:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)registrarme:(id)sender {
    
}

@end
