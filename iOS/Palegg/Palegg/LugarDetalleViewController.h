//
//  LugarDetalleViewController.h
//  Palegg
//
//  Created by Jonathan Fajardo Roa on 31/01/17.
//  Copyright © 2017 Jonathan Fajardo Roa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "empresa.h"
#import "producto.h"

@interface LugarDetalleViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) empresa * EMPRESA;

@property (weak, nonatomic) IBOutlet UITableView *tabla;
@property (weak, nonatomic) IBOutlet UIImageView *img_empresa;
@property (weak, nonatomic) IBOutlet UILabel *lbl_empresa_nombre;
@property (weak, nonatomic) IBOutlet UILabel *lbl_empresa_lema;
@property (weak, nonatomic) IBOutlet UILabel *lbl_empresa_horario;
@property (weak, nonatomic) IBOutlet UILabel *lbl_empresa_valor_entrega;
@property (weak, nonatomic) IBOutlet UIButton *btn_carrito;
- (IBAction)carrito:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lbl_cant_carrito;
@property (weak, nonatomic) IBOutlet UILabel *lbl_total_carrito;
- (IBAction)cerrar:(id)sender;


@end
