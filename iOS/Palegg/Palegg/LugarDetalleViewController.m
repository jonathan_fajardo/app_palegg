//
//  LugarDetalleViewController.m
//  Palegg
//
//  Created by Jonathan Fajardo Roa on 31/01/17.
//  Copyright © 2017 Jonathan Fajardo Roa. All rights reserved.
//

#import "LugarDetalleViewController.h"
#import "CarritoViewController.h"

@interface LugarDetalleViewController ()

@end

@implementation LugarDetalleViewController
@synthesize img_empresa, btn_carrito, tabla, EMPRESA, lbl_cant_carrito, lbl_empresa_lema, lbl_total_carrito, lbl_empresa_nombre, lbl_empresa_horario, lbl_empresa_valor_entrega;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"celda"];
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)carrito:(id)sender {
    CarritoViewController * destino = [self.storyboard instantiateViewControllerWithIdentifier:@"viewCarrito"];
    [self presentViewController:destino animated:YES completion:nil];
}

- (IBAction)cerrar:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
