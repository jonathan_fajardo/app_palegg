//
//  LugaresViewController.h
//  Palegg
//
//  Created by Jonathan Fajardo Roa on 31/01/17.
//  Copyright © 2017 Jonathan Fajardo Roa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "categoria_empresa.h"
#import "empresa.h"
#import "categoria.h"
#import "producto.h"

@interface LugaresViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, NSURLConnectionDelegate>

@property (nonatomic, strong) NSMutableArray * listaColeccionesTabla;

@property (nonatomic, strong) NSMutableData * acumulaDatos;
@property (weak, nonatomic) IBOutlet UIButton *btn_ciudad;
- (IBAction)ciudad:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *tabla;
@property (nonatomic, strong) NSMutableArray * lista_categorias;
@property (weak, nonatomic) IBOutlet UICollectionView *COLLECTIONVIEW_BASE;
@property (weak, nonatomic) IBOutlet UIButton *btn_down_ciudad;
- (IBAction)down_ciudad:(id)sender;

@end
