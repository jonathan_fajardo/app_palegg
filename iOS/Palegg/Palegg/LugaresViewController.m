//
//  LugaresViewController.m
//  Palegg
//
//  Created by Jonathan Fajardo Roa on 31/01/17.
//  Copyright © 2017 Jonathan Fajardo Roa. All rights reserved.
//

#import "LugaresViewController.h"
#import "LugarDetalleViewController.h"

@interface LugaresViewController ()

@end

@implementation LugaresViewController
@synthesize tabla, btn_ciudad, acumulaDatos, lista_categorias, listaColeccionesTabla, COLLECTIONVIEW_BASE, btn_down_ciudad;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    listaColeccionesTabla = [[NSMutableArray alloc]init];
    lista_categorias = [[NSMutableArray alloc]init];
    
    categoria_empresa * CATEGORIA_DECORACION = [[categoria_empresa alloc]init];
    CATEGORIA_DECORACION.categoria_empresa_id = @"1";
    CATEGORIA_DECORACION.categoria_empresa_nombre = @"DECORACION";
    CATEGORIA_DECORACION.categoria_empresa_lista = [[NSMutableArray alloc]init];
    
    empresa * EMPRESA_DECORACION_1 = [[empresa alloc]init];
    EMPRESA_DECORACION_1.empresa_id = @"1";
    EMPRESA_DECORACION_1.empresa_nombre = @"HOME DECORATION";
    EMPRESA_DECORACION_1.empresa_lema = @"Las mejores decoraciones para el hogar";
    EMPRESA_DECORACION_1.empresa_descripcion = @"Las mejores decoraciones para el hogar";
    EMPRESA_DECORACION_1.empresa_horario = @"16:00 - 23:50";
    EMPRESA_DECORACION_1.empresa_valor_entrega = @"2000";
    EMPRESA_DECORACION_1.empresa_direccion = @"Calle 10 # 2 - 45 Centro";
    EMPRESA_DECORACION_1.empresa_telefono = @"3104567899";
    EMPRESA_DECORACION_1.ciudad_id = @"1";
    EMPRESA_DECORACION_1.empresa_img = @"decoracion.jpg";
    EMPRESA_DECORACION_1.empresa_tiempo_entrega = @"20 MIN";
    EMPRESA_DECORACION_1.empresa_categorias = [[NSMutableArray alloc]init];
    EMPRESA_DECORACION_1.empresa_correo = @"info@homedecoration.com";
    [CATEGORIA_DECORACION.categoria_empresa_lista addObject:EMPRESA_DECORACION_1];
    
    empresa * EMPRESA_DECORACION_2 = [[empresa alloc]init];
    EMPRESA_DECORACION_2.empresa_id = @"2";
    EMPRESA_DECORACION_2.empresa_nombre = @"HOME DECORATION 2";
    EMPRESA_DECORACION_2.empresa_lema = @"Las mejores decoraciones para el hogar";
    EMPRESA_DECORACION_2.empresa_descripcion = @"Las mejores decoraciones para el hogar";
    EMPRESA_DECORACION_2.empresa_horario = @"16:00 - 23:50";
    EMPRESA_DECORACION_2.empresa_valor_entrega = @"2000";
    EMPRESA_DECORACION_2.empresa_direccion = @"Calle 10 # 2 - 45 Centro";
    EMPRESA_DECORACION_2.empresa_telefono = @"3104567899";
    EMPRESA_DECORACION_2.ciudad_id = @"1";
    EMPRESA_DECORACION_2.empresa_img = @"decoracion1.jpg";
    EMPRESA_DECORACION_2.empresa_tiempo_entrega = @"20 MIN";
    EMPRESA_DECORACION_2.empresa_categorias = [[NSMutableArray alloc]init];
    EMPRESA_DECORACION_2.empresa_correo = @"info@homedecoration1.com";
    [CATEGORIA_DECORACION.categoria_empresa_lista addObject:EMPRESA_DECORACION_2];
    
    empresa * EMPRESA_DECORACION_3 = [[empresa alloc]init];
    EMPRESA_DECORACION_3.empresa_id = @"3";
    EMPRESA_DECORACION_3.empresa_nombre = @"HOME DECORATION 3";
    EMPRESA_DECORACION_3.empresa_lema = @"Las mejores decoraciones para el hogar";
    EMPRESA_DECORACION_3.empresa_descripcion = @"Las mejores decoraciones para el hogar";
    EMPRESA_DECORACION_3.empresa_horario = @"16:00 - 23:50";
    EMPRESA_DECORACION_3.empresa_valor_entrega = @"2000";
    EMPRESA_DECORACION_3.empresa_direccion = @"Calle 10 # 2 - 45 Centro";
    EMPRESA_DECORACION_3.empresa_telefono = @"3104567899";
    EMPRESA_DECORACION_3.ciudad_id = @"1";
    EMPRESA_DECORACION_3.empresa_img = @"decoracion2.jpg";
    EMPRESA_DECORACION_3.empresa_tiempo_entrega = @"20 MIN";
    EMPRESA_DECORACION_3.empresa_categorias = [[NSMutableArray alloc]init];
    EMPRESA_DECORACION_3.empresa_correo = @"info@homedecoration1.com";
    [CATEGORIA_DECORACION.categoria_empresa_lista addObject:EMPRESA_DECORACION_3];
    
    [listaColeccionesTabla addObject:COLLECTIONVIEW_BASE];
    [lista_categorias addObject:CATEGORIA_DECORACION];
    
    
    
    
    
    
    
    categoria_empresa * CATEGORIA_MODA = [[categoria_empresa alloc]init];
    CATEGORIA_MODA.categoria_empresa_id = @"2";
    CATEGORIA_MODA.categoria_empresa_nombre = @"MODA";
    CATEGORIA_MODA.categoria_empresa_lista = [[NSMutableArray alloc]init];
    
    empresa * EMPRESA_MODA_1 = [[empresa alloc]init];
    EMPRESA_MODA_1.empresa_id = @"1";
    EMPRESA_MODA_1.empresa_nombre = @"MODA 1";
    EMPRESA_MODA_1.empresa_lema = @"La mejor moda de todos los tiempos";
    EMPRESA_MODA_1.empresa_lema = @"La mejor moda de todos los tiempos";
    EMPRESA_MODA_1.empresa_horario = @"08:00 - 22:00";
    EMPRESA_MODA_1.empresa_valor_entrega = @"10000";
    EMPRESA_MODA_1.empresa_direccion = @"Calle 50 # 12 - 45";
    EMPRESA_MODA_1.empresa_telefono = @"3104567899";
    EMPRESA_MODA_1.ciudad_id = @"1";
    EMPRESA_MODA_1.empresa_img = @"moda.jpg";
    EMPRESA_MODA_1.empresa_tiempo_entrega = @"1 DIA";
    EMPRESA_MODA_1.empresa_categorias = [[NSMutableArray alloc]init];
    EMPRESA_MODA_1.empresa_correo = @"info@moda.com";
    [CATEGORIA_MODA.categoria_empresa_lista addObject:EMPRESA_MODA_1];
    
    [listaColeccionesTabla addObject:COLLECTIONVIEW_BASE];
    [lista_categorias addObject:CATEGORIA_MODA];
    
    
    
    [btn_ciudad setTitle:@"NEIVA" forState:UIControlStateNormal];
    [btn_ciudad sizeToFit];
    
    /*btn_ciudad.frame = CGRectMake(self.view.frame.size.width / 2 - (btn_ciudad.frame.size.width / 2 + 8 + btn_down_ciudad.frame.size.width / 2), btn_ciudad.frame.origin.y, btn_ciudad.frame.size.width, btn_ciudad.frame.size.height);*/
    
    btn_ciudad.frame = CGRectMake(self.view.frame.size.width / 2 - btn_ciudad.frame.size.width / 2, btn_ciudad.frame.origin.y, btn_ciudad.frame.size.width, btn_ciudad.frame.size.height);
    
    btn_down_ciudad.frame = CGRectMake(btn_ciudad.frame.origin.x + btn_ciudad.frame.size.width + 8, btn_down_ciudad.frame.origin.y, btn_down_ciudad.frame.size.width, btn_down_ciudad.frame.size.height);
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return lista_categorias.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"celda"];
    
    categoria_empresa * CATEGORIA_EMPRESA = [lista_categorias objectAtIndex:indexPath.row];
    
    UILabel * lbl_categoria = (UILabel *)[cell viewWithTag:10];
    lbl_categoria.text = CATEGORIA_EMPRESA.categoria_empresa_nombre;
    
    UICollectionView * coleccionCanal = (UICollectionView *)[cell viewWithTag:20];
    
    [listaColeccionesTabla removeObjectAtIndex:indexPath.row];
    [listaColeccionesTabla insertObject:coleccionCanal atIndex:indexPath.row];
    
    [coleccionCanal reloadData];
    
    return cell;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    categoria_empresa * CATEGORIA_EMPRESA;
    
    for (int i = 0; i < lista_categorias.count; i++) {
        UICollectionView * col = [listaColeccionesTabla objectAtIndex:i];
        if (col == collectionView) {
            CATEGORIA_EMPRESA = [lista_categorias objectAtIndex:i];
            break;
        }
    }
    
    if (lista_categorias.count > 0) {
        return CATEGORIA_EMPRESA.categoria_empresa_lista.count;
    } else {
        return 0;
    }
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"celda" forIndexPath:indexPath];
    
    for (int i = 0; i < lista_categorias.count; i++) {
        UICollectionView * col = [listaColeccionesTabla objectAtIndex:i];
        if (col == collectionView) {
            
            categoria_empresa * CATEGORIA_EMPRESA = [lista_categorias objectAtIndex:i];
            empresa * EMPRESA = [CATEGORIA_EMPRESA.categoria_empresa_lista objectAtIndex:indexPath.row];
            
            UIImageView * img_empresa = (UIImageView *)[cell viewWithTag:1];
            img_empresa.image = [UIImage imageNamed:EMPRESA.empresa_img];
            
            UILabel * lbl_empresa = (UILabel *)[cell viewWithTag:2];
            lbl_empresa.text = EMPRESA.empresa_nombre;
            
            UILabel * lbl_lema = (UILabel *)[cell viewWithTag:3];
            lbl_lema.text = EMPRESA.empresa_lema;
            
            UILabel * lbl_tiempo = (UILabel *)[cell viewWithTag:4];
            lbl_tiempo.text = EMPRESA.empresa_tiempo_entrega;
            
            break;
        }
    }
    
    
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    LugarDetalleViewController * destino = [self.storyboard instantiateViewControllerWithIdentifier:@"viewLugarDetalle"];
    [self presentViewController:destino animated:YES completion:nil];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(self.view.frame.size.width - 35, 200);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)ciudad:(id)sender {
    
}

- (IBAction)down_ciudad:(id)sender {
    
}

@end
