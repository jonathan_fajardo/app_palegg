//
//  RecordarClaveViewController.h
//  Palegg
//
//  Created by Jonathan Fajardo Roa on 31/01/17.
//  Copyright © 2017 Jonathan Fajardo Roa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordarClaveViewController : UIViewController

@property (nonatomic, strong) NSMutableData * acumulaDatos;
@property(nonatomic, strong)UITextField * campoActivo;

- (IBAction)volver:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txt_correo;
@property (weak, nonatomic) IBOutlet UIButton *btn_recordar_clave;
- (IBAction)recordar_clave:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end
