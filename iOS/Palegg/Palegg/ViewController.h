//
//  ViewController.h
//  Palegg
//
//  Created by Jonathan Fajardo Roa on 31/01/17.
//  Copyright © 2017 Jonathan Fajardo Roa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) NSMutableData * acumulaDatos;
@property(nonatomic, strong)UITextField * campoActivo;

- (IBAction)saltar:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txt_correo;
@property (weak, nonatomic) IBOutlet UITextField *txt_clave;
@property (weak, nonatomic) IBOutlet UIButton *btn_ingresar;
- (IBAction)ingresar:(id)sender;
- (IBAction)recordar_clave:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btn_facebook;
- (IBAction)ingresar_facebook:(id)sender;
- (IBAction)registrarme:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

