//
//  ViewController.m
//  Palegg
//
//  Created by Jonathan Fajardo Roa on 31/01/17.
//  Copyright © 2017 Jonathan Fajardo Roa. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize txt_clave, txt_correo, btn_facebook, btn_ingresar, scrollView, campoActivo;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    txt_correo.leftView = paddingView;
    txt_correo.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    txt_clave.leftView = paddingView1;
    txt_clave.leftViewMode = UITextFieldViewModeAlways;
    
    UIColor *color = [UIColor lightGrayColor];
    txt_correo.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Correo electrónico" attributes:@{NSForegroundColorAttributeName:color}];
    
    txt_clave.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Contraseña" attributes:@{NSForegroundColorAttributeName:color}];
    
    txt_correo.layer.cornerRadius = 10;
    txt_correo.clipsToBounds = YES;
    
    txt_clave.layer.cornerRadius = 10;
    txt_clave.clipsToBounds = YES;
    
    btn_ingresar.layer.cornerRadius = 10;
    btn_ingresar.clipsToBounds = YES;
    
    btn_facebook.layer.cornerRadius = 10;
    btn_facebook.clipsToBounds = YES;
    
    
    
    //Detección de toques en el scroll view
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewPulsado)];
    [tapRecognizer setCancelsTouchesInView:NO];
    [scrollView addGestureRecognizer:tapRecognizer];
    
    [scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
    
    
    //Notificaciones del teclado
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(apareceElTeclado:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(desapareceElTeclado:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

#pragma mark - Notificaciones del teclado
- (void) apareceElTeclado:(NSNotification *)laNotificacion {
    NSDictionary *infoNotificacion = [laNotificacion userInfo];
    CGSize tamanioTeclado = [[infoNotificacion objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, tamanioTeclado.height, 0);
    [scrollView setContentInset:edgeInsets];
    [scrollView setScrollIndicatorInsets:edgeInsets];
    [scrollView scrollRectToVisible:[self campoActivo].frame animated:YES];
    [scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + tamanioTeclado.height)];
}

- (void) desapareceElTeclado:(NSNotification *)laNotificacion {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    [scrollView setContentInset:edgeInsets];
    [scrollView setScrollIndicatorInsets:edgeInsets];
    [UIView commitAnimations];
    
    [scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
    
    [self scrollViewPulsado];
}

- (void) scrollViewPulsado {
    [[self view] endEditing:YES];
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)saltar:(id)sender {
    
}

- (IBAction)ingresar:(id)sender {
    
}

- (IBAction)recordar_clave:(id)sender {
    
}

- (IBAction)ingresar_facebook:(id)sender {
    
}

- (IBAction)registrarme:(id)sender {
    
}

@end
