//
//  categoria.h
//  Palegg
//
//  Created by Jonathan Fajardo Roa on 31/01/17.
//  Copyright © 2017 Jonathan Fajardo Roa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface categoria : NSObject

@property (nonatomic, strong) NSString * categoria_id;
@property (nonatomic, strong) NSString * categoria_nombre;
@property (nonatomic, strong) NSString * categoria_img;
@property (nonatomic, strong) NSString * categoria_img_estado;
@property (nonatomic, strong) NSString * categoria_img_descargando;
@property (nonatomic, strong) NSData * categoria_img_datos;
@property (nonatomic, strong) NSMutableArray * categoria_productos;

@end
