//
//  categoria_empresa.h
//  Palegg
//
//  Created by Jonathan Fajardo Roa on 31/01/17.
//  Copyright © 2017 Jonathan Fajardo Roa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface categoria_empresa : NSObject

@property (nonatomic, strong) NSString * categoria_empresa_id;
@property (nonatomic, strong) NSString * categoria_empresa_nombre;
@property (nonatomic, strong) NSMutableArray * categoria_empresa_lista;

@end
