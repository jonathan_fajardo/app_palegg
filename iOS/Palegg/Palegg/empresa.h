//
//  empresa.h
//  Palegg
//
//  Created by Jonathan Fajardo Roa on 31/01/17.
//  Copyright © 2017 Jonathan Fajardo Roa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface empresa : NSObject

@property (nonatomic, strong) NSString * empresa_id;
@property (nonatomic, strong) NSString * empresa_nombre;
@property (nonatomic, strong) NSString * empresa_descripcion;
@property (nonatomic, strong) NSString * empresa_lema;
@property (nonatomic, strong) NSString * empresa_latitud;
@property (nonatomic, strong) NSString * empresa_longitud;
@property (nonatomic, strong) NSString * empresa_img;
@property (nonatomic, strong) NSString * empresa_img_estado;
@property (nonatomic, strong) NSString * empresa_img_descargando;
@property (nonatomic, strong) NSData * empresa_img_datos;
@property (nonatomic, strong) NSString * empresa_telefono;
@property (nonatomic, strong) NSString * empresa_direccion;
@property (nonatomic, strong) NSString * empresa_horario;
@property (nonatomic, strong) NSString * empresa_correo;
@property (nonatomic, strong) NSString * empresa_tiempo_entrega;
@property (nonatomic, strong) NSString * empresa_valor_entrega;
@property (nonatomic, strong) NSString * empresa_estado;
@property (nonatomic, strong) NSString * ciudad_id;
@property (nonatomic, strong) NSMutableArray * empresa_categorias;

@end
