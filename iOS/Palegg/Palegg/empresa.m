//
//  empresa.m
//  Palegg
//
//  Created by Jonathan Fajardo Roa on 31/01/17.
//  Copyright © 2017 Jonathan Fajardo Roa. All rights reserved.
//

#import "empresa.h"

@implementation empresa
@synthesize empresa_id, empresa_img, empresa_lema, empresa_correo, empresa_estado, empresa_nombre, empresa_horario, empresa_latitud, empresa_longitud, empresa_telefono, empresa_direccion, empresa_img_datos, empresa_categorias, empresa_img_estado, empresa_descripcion, empresa_valor_entrega, empresa_tiempo_entrega, empresa_img_descargando, ciudad_id;

@end
