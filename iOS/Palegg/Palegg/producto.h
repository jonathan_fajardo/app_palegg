//
//  producto.h
//  Palegg
//
//  Created by Jonathan Fajardo Roa on 31/01/17.
//  Copyright © 2017 Jonathan Fajardo Roa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface producto : NSObject

@property (nonatomic, strong) NSString * producto_id;
@property (nonatomic, strong) NSString * producto_nombre;
@property (nonatomic, strong) NSString * producto_descripcion;
@property (nonatomic, strong) NSString * producto_valor;
@property (nonatomic, strong) NSString * producto_img;
@property (nonatomic, strong) NSString * producto_img_estado;
@property (nonatomic, strong) NSString * producto_img_descargando;
@property (nonatomic, strong) NSData * producto_img_datos;
@property (nonatomic, strong) NSString * categoria_id;

@end
